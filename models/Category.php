<?php
namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class Category
 * @package app\models
 *
 * @property int $id
 * @property string $name
 */
class Category extends ActiveRecord
{

    public static function tableName()
    {
        return 'category';
    }

    public function rules()
    {
        return [
            [['name'], 'trim'],

            [['id', 'name'], 'required'],

            [['id'], 'int'],
            [['name'], 'string']
        ];
    }

}