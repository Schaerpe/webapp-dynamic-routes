<?php
namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class Town
 * @package app\models
 *
 * @property int $id
 * @property string $name
 * @property int $region_id
 *
 * @property string $region
 */
class Town extends ActiveRecord
{

    public static function tableName()
    {
        return 'town';
    }

    public function rules()
    {
        return [
            [['name'], 'trim'],

            [['id', 'region_id', 'name'], 'required'],

            [['id', 'region_id'], 'int'],
            [['name'], 'string']
        ];
    }

    public function getRegion()
    {
        return $this->hasOne(Region::class, ['id'=>'region_id']);
    }

}