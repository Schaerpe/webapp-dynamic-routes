<?php
namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class Region
 * @package app\models
 *
 * @property int $id
 * @property string $name
 *
 * @property Town[] $towns
 */
class Region extends ActiveRecord
{

    public static function tableName()
    {
        return 'region';
    }

    public function rules()
    {
        return [
            [['name'], 'trim'],

            [['id', 'name'], 'required'],

            [['id'], 'int'],
            [['name'], 'string']
        ];
    }

    public function getTowns()
    {
        return $this->hasMany(Town::class, ['region_id'=>'id']);
    }

}