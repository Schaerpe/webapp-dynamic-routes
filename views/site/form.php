<?php

/** @var \yii\base\View $this */
/** @var \app\models\Category $category */
/** @var \app\models\Region $region */
/** @var \app\models\Town $town */
?>

<h1><?= $category->name ?> Offertenformular</h1>
<table class="table">
    <thead>
        <tr>
            <th>Kanton</th>
            <th>Gemeinde</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?= $region->name ?></td>
            <td><?= $town->name ?></td>
        </tr>
    </tbody>
</table>
