<?php
/* @var $this \yii\web\View */

use yii\helpers\Html;

/* @var $categories \app\models\Category[] */
/* @var $regions \app\models\Region[] */
?>

<?php foreach($categories as $category): ?>
<h2><?= $category->name ?></h2>
<ul>
    <?php foreach($regions as $region): ?>
        <li>
            <h3><?= $region->name ?></h3>
            <ul>
                <?php foreach($region->towns  as $town): ?>
                    <?php $url = sprintf('form/%s/%s/%s', $category->name, $region->name, $town->name) ?>
                    <li><?= Html::a($town->name, [$url]) ?></li>
                <?php endforeach; ?>
            </ul>
        </li>
    <?php endforeach; ?>
</ul>
<?php endforeach; ?>
