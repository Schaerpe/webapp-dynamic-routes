<?php

namespace app\controllers;

use app\models\Category;
use app\models\Region;
use app\models\Town;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionTree()
    {
        return $this->render('tree', [
            'categories'=>Category::find()->all(),
            'regions'=>Region::find()->all()
        ]);
    }

    public function actionForm($category, $region, $town)
    {
        return $this->render('form', [
            'category'=>Category::find()->andWhere(['category.name'=>$category])->one(),
            'region'=>Region::find()->andWhere(['region.name'=>$region])->one(),
            'town'=>Town::find()->andWhere(['town.name'=>$town])->one(),
        ]);
    }

}
